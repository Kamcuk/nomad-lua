MAKEFLAGS = -rR --warn-unused-variables
SHELL = bash

version = $(shell lua -e 'loadfile("nomad-lua.rockspec")() print(version)')
rockspec = nomad-lua-$(version).rockspec

all: make

make: $(rockspec)
	luarocks make --local $(rockspec)

pack: $(rockspec)
	luarocks pack --local $(rockspec)

upload: pack
	luarocks upload --local $(rockspec)

rockspec: $(rockspec)
$(rockspec): nomad-lua.rockspec
	cp -v nomad-lua.rockspec nomad-lua-$(version).rockspec


test:
	lua ./lua/nomad-lua.lua ./tests/nomadjob.example.lua

lint:
	find -name *.lua  | xargs -t luacheck
