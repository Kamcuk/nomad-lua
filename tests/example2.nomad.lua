---@type string
local jobname = "nomad-lua-example2"

---@param name string
---@param script string
---@return Task
local function gen_task(name, script)
	return {
		Name = name,
		Driver = "docker",
		config = {
			image = "busybox",
			args = {
				"sh",
				"-xc",
				script,
			},
		},
	}
end

---@type Job
local job = {
	ID = jobname,
	Type = "batch",
	TaskGroups = {
		{
			Name = jobname,
			RestartPolicy = {
				Attempts = 0,
			},
			ReschedulePolicy = {
				Attempts = 0,
			},
			Tasks = {
				gen_task("1", [[ echo 123 ]]),
				gen_task("2", [[ echo 234 ]]),
			},
		},
	},
}

return job
