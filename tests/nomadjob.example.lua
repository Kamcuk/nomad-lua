---@type Job
local job = {
    ID="nomad-lua-example",
    Type="batch",
    TaskGroups={
        {
            Name="nomad-lua-example",
            RestartPolicy={
                Attempts=0,
            },
            ReschedulePolicy={
                Attempts=0,
            },
            Tasks={
                {
                    Name="nomad-lua-example",
                    Driver="docker",
                    config={
                        image="busybox",
                        args={
                            "sh",
                            "-xc",
                            "echo hello world",
                        }
                    }
                }
            }
        }
    }
}
return job
