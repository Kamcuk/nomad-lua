-- nomad-lua.lua

local argparse = require("argparse")
local inspect = require("inspect")
local cjson = require("cjson")

-- Require to export to dofile.
---@diagnostic disable: lowercase-global
nomadlib = require("nomadlua")

local function quote(s)
	return "'" .. s:gsub("'", "'\\''") .. "'"
end

---@class log
---@field lvl number
local log = { lvl = 0 }

function log.log(s)
    io.stderr:write(("%s: %s\n"):format("nomad-lua", s))
end


---@param s string
function log.debug(s)
	if log.lvl >= 1 then
		log.log(s)
	end
end

---@param s string
function log.info(s)
	log.log(s)
end

local function addif(to, val, str)
	return to .. (val and " " .. str or "")
end

local function main()
	-- Parse args
	local parser = argparse()({
		name = "nomad-lua",
		description = "Call nomad job run by generating nomad job specification from lua module",
		epilog = "Written by Kamil Cukrowski 2023",
	})
	parser:flag("-d --debug")
	parser:flag("--output", "Pass -output to nomad")
	parser:argument("command"):choices({ "run", "print" })
	parser:argument("input", "input file.")
	local args = parser:parse()
    --
	log.lvl = args.debug and 1 or 0
	-- Load nomad job file
	log.debug(inspect(args))
	local nomadjob = dofile(args.input)
	local nomadjobjson = cjson.encode({ Job = nomadjob })
	if args.command == "print" then
		print(nomadjobjson)
	elseif args.command == "run" then
		-- construct nomad command
		local cmd = "nomad job run -json"
		cmd = addif(cmd, args.output, "-output")
		cmd = cmd .. " " .. "-"
		log.info("+ " .. cmd)
		-- run nomad command
		local h = assert(io.popen(cmd, "w"))
		h:write(nomadjobjson)
		local rc = { h:close() }
		log.debug("rc = " .. inspect(rc))
		os.exit(rc[3])
	else
		assert(nil)
	end
end

main()
