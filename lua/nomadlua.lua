---
local M = {}

---@alias VolumeOptions {no_copy: boolean, driver_config: {options: {type: string, device: string, o: string}}}

---@class Mount
---@field type string?
---@field source string?
---@field target string
---@field readonly string
---@field volume_options VolumeOptions?

---@class DockerConfig
---@field image string
---@field args string[]
---@field mounts Mount[]?

---@class Task
---@field Name string
---@field Driver string
---@field config DockerConfig

---@class ReschedulePolicy
---@field Attempts integer

---@class RestartPolicy
---@field Attempts integer

---@class TaskGroup
---@field Name string
---@field Count integer?
---@field Meta table<string, string>?
---@field Tasks Task[]
---@field ReschedulePolicy ReschedulePolicy?
---@field RestartPolicy RestartPolicy?

---@class Job
---@field ID string
---@field Type string?
---@field TaskGroups TaskGroup[]
---@field Meta table<string, string>?

---@param source string
---@param target string
---@param readonly boolean?
---@return Mount
function M.mount_bind(source, target, readonly)
	return {
		type = "bind",
		source = source,
		target = target,
		readonly = readonly or false,
	}
end

---@param server string
---@param path string
---@param target string
---@param options string?
---@param readonly boolean?
---@return Mount
function M.mount_nfs(server, path, target, options, readonly)
	readonly = readonly == nil and false or readonly
	return {
		target = target,
		readonly = readonly or false,
		volume_options = {
			no_copy = true,
			driver_config = {
				options = {
					type = "nfs",
					device = server .. ":" .. path,
					o = options or (readonly and "ro" or "rw"),
				},
			},
		},
	}
end

return M
