# nomad-lua

A Lua program to execute a Lua script that returns Lua table of Nomad job
specification and then executes `nomad job run -json -` with it.

Install with luarocks:

```
luarocks install nomad-lua
```

Remember to load luarocks: `. "$(luarocks path)"`
