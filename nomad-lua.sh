#!/bin/sh
set -eu
cd "$(dirname "$(readlink -f "$0")")"
set -x
env LUA_PATH="$PWD/lua/?.lua${LUA_PATH:+;$LUA_PATH}" lua -W ./lua/nomad-lua.lua -d "$@"
